/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstpushback.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccellado <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 16:48:07 by ccellado          #+#    #+#             */
/*   Updated: 2018/12/11 17:16:11 by ccellado         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstpushback(t_list **start, t_list *list)
{
	t_list *iter;

	if (*start == 0 && *start && start)
		*start = list;
	else if (*start && start)
	{
		iter = *start;
		while (iter->next != 0)
			iter = iter->next;
		iter->next = list;
	}
}
