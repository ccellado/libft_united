/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccellado <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 16:44:27 by ccellado          #+#    #+#             */
/*   Updated: 2018/12/11 17:16:42 by ccellado         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstrev(t_list **start)
{
	t_list	*previous;
	t_list	*current;
	t_list	*next;

	if (start == 0 || *start == 0 || (*start)->next == 0)
		return ;
	previous = 0;
	current = *start;
	while (current != 0)
	{
		next = current->next;
		current->next = previous;
		previous = current;
		current = next;
	}
	*start = previous;
}
