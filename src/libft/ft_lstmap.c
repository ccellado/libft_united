/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccellado <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 08:25:58 by ccellado          #+#    #+#             */
/*   Updated: 2018/12/11 17:13:55 by ccellado         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list		*new;
	t_list		*list;

	if (!lst || !f || !(list = f(ft_lstnew(lst->content, lst->content_size))))
		return (NULL);
	new = list;
	while (lst->next)
	{
		lst = lst->next;
		if (!(list->next = f(ft_lstnew(lst->content, lst->content_size))))
		{
			ft_lstclear((t_list **)new);
			return (NULL);
		}
		list = list->next;
	}
	return (new);
}
