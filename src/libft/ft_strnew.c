/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccellado <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/26 05:35:40 by ccellado          #+#    #+#             */
/*   Updated: 2018/12/11 16:32:02 by ccellado         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnew(size_t size)
{
	char	*all;

	if (!(all = (char *)malloc(sizeof(char) * size + 1)))
		return (NULL);
	ft_bzero(all, size + 1);
	return (all);
}
