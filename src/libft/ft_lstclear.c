/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstclear.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccellado <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 17:10:46 by ccellado          #+#    #+#             */
/*   Updated: 2019/09/17 19:20:26 by ccellado         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstclear(t_list **start)
{
	t_list *iter;
	t_list *next;

	iter = *start;
	while (iter != 0)
	{
		next = iter->next;
		free(iter);
		iter = next;
	}
	*start = 0;
}
