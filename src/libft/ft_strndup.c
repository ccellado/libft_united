/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccellado <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 15:31:11 by ccellado          #+#    #+#             */
/*   Updated: 2018/12/11 16:40:52 by ccellado         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strndup(char *source, int n)
{
	char	*res;
	int		i;

	if (!(res = malloc(n + 1)))
		return (0);
	i = 0;
	while (i < n)
		res[i++] = *source++;
	res[i] = 0;
	return (res);
}
